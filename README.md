# Kids.Micro 1 Docs

Tinusaur Board Kids.Micro Gen:1 Documentation

## Tinusaur Board Kids.Micro Assembling Guide

![Tinusaur Board Kids.Micro Assembling Guide](Tinusaur_KidsMicro_1_Assembling_Guide.jpg "Tinusaur Board Kids.Micro Assembling Guide")

- Download link: https://gitlab.com/tinusaur/kidsmicro-1-docs/-/raw/master/Tinusaur_KidsMicro_1_Assembling_Guide.pdf?inline=false

